<?php

use Illuminate\Database\Seeder;
use App\BelajarRedisModel;

class BelajarRedisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paragraf = "Cache adalah suatu proses memyimpan data sementara sehingga situs, browser, atau aplikasi tidak perlu mengunduh ulang datanya. Dengan proses itu akan mempercepat proses membuat tampilan. Salah satu yang bisa digunakan untuk cache adalah Redis. Redis, singkatan dari Remote Dictionary Server, adalah tempat penyimpanan dalam memori yang merupakan software open-source dan bisa digunakan untuk database, cache, dan lain sebagainya.";
        $array = [];
        $array = explode(" ", $paragraf);

        for($i=0; $i<count($array); $i++){
            BelajarRedisModel::create([
                'kata' => $array[$i]
            ]);
        }
    }
}
