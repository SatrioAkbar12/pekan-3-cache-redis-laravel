<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BelajarRedisModel extends Model
{
    protected $table = 'belajar_redis';

    protected $fillable = ['kata'];
}
