<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BelajarRedisModel;
use Illuminate\Support\Facades\Cache;

class ParagrafController extends Controller
{
    public function query(){
        $data = BelajarRedisModel::get('kata');

        foreach($data as $d){
            $array[] = $d['kata'];
        }

        return view('view', ['method' => 'query', 'paragraf' => implode(" ",$array)]);
    }

    public function cache(){
        $data = Cache::remember('data', 10*60, function () {
            return BelajarRedisModel::get('kata');
        });

        foreach($data as $d){
            $array[] = $d['kata'];
        }

        return view('view', ['method' => 'cache', 'paragraf' => implode(" ",$array)]);
    }
}
